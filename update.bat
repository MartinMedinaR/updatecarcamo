@echo off
title Actualizar Yolenith Software
color F0
@echo ---------------------------------------------------------------------------------
@echo           Actualizado el sistema. Yolenith Software. Martin Medina
@echo ----------------------------------------�----------------------------------------



:: todas las posibles carpetas donde puede estar el sistema
set raiz="c:\carcamo"

:: nombres de las carpetas del sistema
set nReportes="reportes"
set nRecursos="recursos"
set nRp="rp"
set nSetup="setup"
set nLog="log"
set nFile="file"

:: archivos del sistema
set dSetup="SetupService.bat"

:: carpetas del sistema
set reportes=%raiz%\%nReportes%
set recursos=%raiz%\%nRecursos%


set rp=%reportes%\%nRp%

set setup=%raiz%\%nSetup%

set log=%raiz%\%nLog%

set fileSetup=%raiz%\%nFile%\%dSetup%

:: se copia los archivo raiz
if exist %raiz% copy *.* %raiz% 


:: se copia todos los archivos de reportes
if exist %reportes% copy %nReportes% %reportes%

:: se copia todos los archivos de recursos
if exist %recursos% copy %nRecursos% %recursos%

:: se copia todos los archivos de rp
if exist %rp% copy %nRp% %rp%

:: se copia todos los archivos de setup
if exist %setup% copy %nSetup% %setup%


:: eliminar rastro del archivo .bat y vbs que se copia al sistema
if exist %raiz% cd %raiz%
if exist *.bat del *.bat
if exist *.vbs del *.vbs

:: comprovar si existen los directorios
if not exist %raiz% @echo Error Verifique que exista el siguiente directorio: %raiz%

:: crear las carpetas en la raiz si no existen
if not exist %reportes% md %reportes%

if not exist %recursos% md %recursos%

if not exist %rp% md %rp%

if not exist %setup% md %setup%

if not exist %log% md %log%

:: abrir el archivo de setup para actualizar la base de datos
if exist %fileSetup% call %fileSetup%


pause>nul 
exit
